+++
title = "Homepage"
description = "About me"
+++

<center>

<img src="me.png"  width=35% > &nbsp; 

I am a Artificial Intelligence Engineer at [**Micropep Technologies**](https://micro-pep.com), a plant biotechnology company. My background lies at the intersection of mathematics and theoretical and applied computer science. I specialized in discrete optimization for computational biology applications during my Ph.D.

&nbsp;

You can find my resume [here](resume.pdf).


</center>
