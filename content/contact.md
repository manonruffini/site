+++
title = "Contact"
description = "Contact me"
+++

<img src="linkedin.svg" style="float: left;" width=30 > &nbsp; &nbsp;  [manon-ruffini](https://linkedin.com/in/manon-ruffini)

<img src="email.svg" style="float: left;" width=30 > &nbsp; &nbsp;  [ruffini.manon@gmail.com](mailto:ruffini.manon@gmail.com)