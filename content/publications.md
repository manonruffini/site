+++
title = "Publications"
description = "List of publications"
aliases = ["publications", "article", "scientific-contributions"]
+++

<sub>

## Published papers

</style>
 - [Computational Design of mini-protein binders](https://europepmc.org/article/med/35298822)

    Younes Bouchiba, *Manon Ruffini*, Thomas Schiex, Sophie Barbe (2022)
    
    In Computational Peptide Science (pp. 361-382). Humana, New York, NY.

 - [Guaranteed Diversity and Optimality in Cost Function Network Based Computational Protein Design Methods](https://www.mdpi.com/1999-4893/14/6/168)
  
   *Manon Ruffini*, Jelena Vučinić, Simon de Givry, George Katsirelos, Sophie Barbe, Thomas Schiex (2021)

   Algorithms, June 2021. [journal cover](https://www.mdpi.com/files/uploaded/covers/algorithms/big_cover-algorithms-v14-i6.png)

 - [Guaranteed Diversity & Quality for the Weighted CSP](https://miat.inrae.fr/schiex/Export/diverseICTAI.pdf)

    *Manon Ruffini*, Jelena Vučinić, Simon de Givry, George Katsirelos, Sophie Barbe, Thomas Schiex (2019)

    2019 IEEE 31st international conference on tools with artificial intelligence (ICTAI). IEEE, 2019. p. 18-25.

 - [Positive Multi-State Protein Design](https://academic.oup.com/bioinformatics/article/36/1/122/5519117)
  
    Jelena Vučinić, David Simoncini, *Manon Ruffini*, Sophie Barbe, Thomas Schiex (2019)

    Bioinformatics, 2020, vol. 36, no 1, p. 122-130.


## Ph.D. thesis

[Models and Algorithms for Computational Protein Design](manuscript.pdf)

</sub>